unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Parser;

type
  TMainForm = class(TForm)
    edtInfix: TLabeledEdit;
    edtPostfix: TLabeledEdit;
    btnTransform: TButton;
    lblRank: TLabel;
    procedure btnTransformClick(Sender: TObject);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnTransformClick(Sender: TObject);
var
   PostfixNotation: string;
begin
   MyParser := TParser.Create();

   //a^b+c^(d+s)^f
   //((a+b)*c-d^x^y)/(m+n)
   //(a+b+c*(y-g*d)^n^k+s*l)/(x-f*t*p+w)
   //w*a+b+c*(y-g*d)^(k+n)^k+s*l/s*l/(x-f*t*p+w)

   //(a+b*c*(y-g*d)^n^(k+s)*l)/(f*(d+g*h)/x-f*t*p+w)

   PostfixNotation := MyParser.InfixToPostfix(edtInfix.Text);
   edtPostfix.Text := PostfixNotation;

   lblRank.Caption := 'Rank: ' + IntToStr(MyParser.GetRank(PostfixNotation));
end;

end.
