program ReversePolishNotation;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  Parser in 'Parser.pas',
  Stack in 'Stack.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
