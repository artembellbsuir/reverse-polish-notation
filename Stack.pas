unit Stack;

interface

uses
   Generics.Collections;

type
   TMap = TDictionary<Char, Integer>;
   TOperatorsStack = TStack<Char>;

   TCoolStack = class
   private
      RelativePrecedence, StackPrecedence: TMap;
      Stack: TOperatorsStack;

      function GetTopChar: Char;
   public
      constructor Create;
      function Push(C: Char): string;
      function GetLeftElements: string;
   end;

const
   Letters = ['A' .. 'Z', 'a' .. 'z'];

implementation

{ TCoolStack }

constructor TCoolStack.Create;
var
   i: Integer;
   Temp: Char;
begin
   with Self do
   begin
      // RELATIVE
      RelativePrecedence := TMap.Create;
      RelativePrecedence.Add('+', 1);
      RelativePrecedence.Add('-', 1);
      RelativePrecedence.Add('*', 3);
      RelativePrecedence.Add('/', 3);
      RelativePrecedence.Add('^', 6);
      for Temp in Letters do
         RelativePrecedence.Add(Temp, 7);
      RelativePrecedence.Add('(', 9);
      RelativePrecedence.Add(')', 0);

      // STACK
      StackPrecedence := TMap.Create;
      StackPrecedence.Add('+', 2);
      StackPrecedence.Add('-', 2);
      StackPrecedence.Add('*', 4);
      StackPrecedence.Add('/', 4);
      StackPrecedence.Add('^', 5);
      for Temp in Letters do
         StackPrecedence.Add(Temp, 8);
      StackPrecedence.Add('(', 0);

      Stack := TOperatorsStack.Create;
   end;
end;

function TCoolStack.GetLeftElements: string;
begin
   while Stack.Count <> 0 do
      Result := Result + Stack.Pop;
end;

function TCoolStack.GetTopChar: Char;
begin
   Result := ' ';
   if Stack.Count <> 0 then
      Result := Stack.Peek;
end;

function TCoolStack.Push(C: Char): string;
var
   TopChar: Char;
begin

   Result := '';

   TopChar := GetTopChar;

   if C = ')' then
   begin
      while TopChar <> '(' do
      begin
         Result := Result + Stack.Pop;
         TopChar := GetTopChar;
      end;
      Stack.Pop;
   end
   else if (Stack.Count = 0) or
     (StackPrecedence[TopChar] < RelativePrecedence[C]) then
      Stack.Push(C)
   else
   begin

      while (Stack.Count <> 0) and
        (StackPrecedence[TopChar] > RelativePrecedence[C]) do
      begin
         Result := Result + Stack.Pop;
         TopChar := GetTopChar
      end;
      Stack.Push(C);
   end;
end;

end.
