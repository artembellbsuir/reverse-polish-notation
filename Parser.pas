unit Parser;

interface

uses
   Generics.Collections, Stack;

type
   TParser = class(TObject)
   private
      Stack: TCoolStack;
   public
      constructor Create;
      function InfixToPostfix(const Src: string): string;
      function GetRank(const Src: string): Integer;
   end;

var
   MyParser: TParser;

implementation

{ TParser }

constructor TParser.Create;
begin
   Stack := TCoolStack.Create;
end;

function TParser.GetRank(const Src: string): Integer;
var
   i: Integer;
begin
   Result := 0;
   for i := 1 to Length(Src) do
      if Src[i] in Letters then
         Inc(Result)
      else
         Dec(Result);
end;

function TParser.InfixToPostfix(const Src: string): string;
var
   i: Integer;
   Temp: Char;
   TempString: string;
begin
   for i := 1 to Length(Src) do
   begin
      Temp := Src[i];
      TempString := Stack.Push(Temp);
      Result := Result + TempString;
   end;

   Result := Result + Stack.GetLeftElements;
end;

end.
